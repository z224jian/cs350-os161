#include <types.h>
#include <lib.h>
#include <synchprobs.h>
#include <synch.h>
#include <opt-A1.h>

/* 
 * This simple default synchronization mechanism allows only vehicle at a time
 * into the intersection.   The intersectionSem is used as a a lock.
 * We use a semaphore rather than a lock so that this code will work even
 * before locks are implemented.
 */

/* 
 * Replace this default synchronization mechanism with your own (better) mechanism
 * needed for your solution.   Your mechanism may use any of the available synchronzation
 * primitives, e.g., semaphores, locks, condition variables.   You are also free to 
 * declare other global variables if your solution requires them.
 */

/*
 * replace this with declarations of any synchronization and other variables you need here
 */
// 4 cv, guarding each source direction
// just need to make sure we let
static volatile int count[4];
static volatile Direction queue[4];
static volatile bool in_queue[4];
static volatile int begin;
static volatile int end;
static volatile int left;
static struct cv* dir[4];
static struct lock* lk;

/* 
 * The simulation driver will call this function once before starting
 * the simulation
 *
 * You can use it to initialize synchronization and other variables.
 * 
 */
void
intersection_sync_init(void)
{
  /* replace this default implementation with your own implementation */
  for (int i = 0; i < 4; ++i) {
    count[i] = 0;
    queue[i] = 0;
    in_queue[i] = false;
  }
  dir[0] = cv_create("0");
  dir[1] = cv_create("1");
  dir[2] = cv_create("2");
  dir[3] = cv_create("3");
  lk=lock_create("mutex");
  begin = 0;
  end = 0;
  left = 0;

  return;
}

/* 
 * The simulation driver will call this function once after
 * the simulation has finished
 *
 * You can use it to clean up any synchronization and other variables.
 *
 */
void
intersection_sync_cleanup(void)
{
  /* replace this default implementation with your own implementation */
  for (int i = 0; i < 4; ++i) {
    cv_destroy(dir[i]);
  }
  lock_destroy(lk);
}

void
intersection_before_entry(Direction origin, Direction destination) 
{
  /* replace this default implementation with your own implementation */
  (void)origin;  /* avoid compiler complaint about unused parameter */
  (void)destination; /* avoid compiler complaint about unused parameter */

  lock_acquire(lk);
  // cases:
  // 1. if it's the first 
  // 2. if it's second or after
  ++count[origin];
  if (!left) {
    left = count[origin];
  } else {
    // push to wait queue
    if (!in_queue[origin]) {
      in_queue[origin] = true;
      queue[end] = origin;
      end = (end+1)%4;
    }
    cv_wait(dir[origin], lk);
  }
  lock_release(lk);
}




/*
 * The simulation driver will call this function each time a vehicle
 * leaves the intersection.
 *
 * parameters:
 *    * origin: the Direction from which the vehicle arrived
 *    * destination: the Direction in which the vehicle is going
 *
 * return value: none
 */

static Direction next_direction() {
    Direction next = queue[begin];
    if (in_queue[next]) {
      begin = (begin+1)%4;
      in_queue[next] = false;
      return next;
    }
    return 0;
}

void
intersection_after_exit(Direction origin, Direction destination) 
{
  /* replace this default implementation with your own implementation */
  (void)origin;  /* avoid compiler complaint about unused parameter */
  (void)destination; /* avoid compiler complaint about unused par */
  // KASSERT(origin == (unsigned int)current);
  lock_acquire(lk);
  --left;
  --count[origin];
  if (!left) {
    Direction next_dir = next_direction();
    left = count[next_dir];
    //kprintf("%d cars next turn, from %d\n", left, next_dir);
    if (left) {
      // only broadcast if there are cars waiting
      cv_broadcast(dir[next_dir], lk);
    }
  }
  lock_release(lk);
}
