#include <types.h>
#include <kern/errno.h>
#include <kern/unistd.h>
#include <kern/wait.h>
#include <kern/fcntl.h>
#include <lib.h>
#include <syscall.h>
#include <current.h>
#include <proc.h>
#include <thread.h>
#include <synch.h>
#include <vfs.h>
#include <addrspace.h>
#include <copyinout.h>
#include "../arch/mips/include/trapframe.h"



  /* this implementation of sys__exit does not do anything with the exit code */
  /* this needs to be fixed to get exit() and waitpid() working properly */

void sys__exit(int exitcode) {

  struct addrspace *as;
  struct proc *p = curproc;
  /* for now, just include this to keep the compiler from complaining about
     an unused variable */
  (void)exitcode;
#if OPT_A2
  lock_acquire(p->p_exit_lock);
#endif

  DEBUG(DB_SYSCALL,"Syscall: _exit(%d)\n",exitcode);

  KASSERT(curproc->p_addrspace != NULL);
  as_deactivate();
  /*
   * clear p_addrspace before calling as_destroy. Otherwise if
   * as_destroy sleeps (which is quite possible) when we
   * come back we'll be calling as_activate on a
   * half-destroyed address space. This tends to be
   * messily fatal.
   */
  as = curproc_setas(NULL);
  as_destroy(as);

  /* detach this thread from its process */
  /* note: curproc cannot be used after this call */
  proc_remthread(curthread);

#if OPT_A2
  p->p_alive = false;
  p->p_exitcode = _MKWAIT_EXIT(exitcode);

  // See if the children can be deleted
  // Deletion of children is the responsibility of parent
  // When parent exits, parent will delete the already dead children, and
  // give p->p_parent to be NULL on alive child processes
  // Two cases for a given child process p
  // 1. If p is already dead (i.e., finished its sys__exit + thread_exit), run
  //    proc_destroy on that p, since parent is exiting and when p exited,
  //    its parent wasn't dead, so it didn't delete itself
  // 2. If p is not dead, then it would become an orphan process after
  //    its parent is deleted. In this case, mark its parent pointer to be
  //    NULL, so that when it calls sys__exit, it will detect a NULL parent ptr,
  //    and then it would be safe to delete p since it's an orphan process.

  unsigned int child_size = array_num(p->p_children), idx = 0, count = 0;

  // Mark orphan child processes to have NULL parent process
  for (unsigned int i = 0; i < child_size; ++i) {
    struct proc *child_proc = (struct proc *)array_get(p->p_children, i);
    child_proc->p_parent = NULL;
  }

  // Destroy dead orphan processes
  for (count = 0; count < child_size; ++count) {
      struct proc *child_proc = (struct proc *)array_get(p->p_children, idx);
      // Acquire child process lock, to make sure we don't have race conditions
      // when the child process is exiting
      lock_acquire(child_proc->p_exit_lock);
      bool child_proc_exited = !child_proc->p_alive;
      lock_release(child_proc->p_exit_lock);
      if (child_proc_exited) {
        proc_destroy(child_proc);
        array_remove(p->p_children, idx);
      }
      ++idx;
  }

  if (p->p_parent != NULL) {
    // If parent is not NULL, i.e., its parent is still alive, then wake parent up
    // since it might have called waitpid on the child
    cv_signal(p->p_exit_cv, p->p_exit_lock);
    lock_release(p->p_exit_lock);
  } else {
    // Parent is NULL, in this case, the child process is an orphan process,
    // we can safely delete it since no one would call waitpid on it anymore
    lock_release(p->p_exit_lock);
    proc_destroy(p);
  }

#else
  /* if this is the last user process in the system, proc_destroy()
     will wake up the kernel menu thread */
  proc_destroy(p);

#endif
  
  thread_exit();
  /* thread_exit() does not return, so we should never get here */
  panic("return from thread_exit in sys_exit\n");
}


/* stub handler for getpid() system call                */
int
sys_getpid(pid_t *retval)
{
  /* for now, this is just a stub that always returns a PID of 1 */
  /* you need to fix this to make it work properly */
#if OPT_A2
  *retval = curproc->pid;
#else
  *retval = 1;
#endif
  return(0);
}

/* stub handler for waitpid() system call                */

int
sys_waitpid(pid_t pid,
	    userptr_t status,
	    int options,
	    pid_t *retval)
{
  int exitstatus;
  int result;
  struct proc *p = curproc;

  /* this is just a stub implementation that always reports an
     exit status of 0, regardless of the actual exit status of
     the specified process.   
     In fact, this will return 0 even if the specified process
     is still running, and even if it never existed in the first place.

     Fix this!
  */

  if (options != 0) {
    return(EINVAL);
  }

#if OPT_A2
  unsigned int child_idx = 0, child_size = array_num(p->p_children);
  for (child_idx = 0; child_idx < child_size; ++child_idx) {
    struct proc *child_proc = (struct proc *)array_get(p->p_children, child_idx);
    if (child_proc->pid == pid) break;
  }
#endif

  /* for now, just pretend the exitstatus is 0 */
  exitstatus = 0;

#if OPT_A2
  // Modify exit status and wait (if necessary) to see exitcode of child process
  if (child_idx < child_size) {
    struct proc *child_proc = (struct proc *)array_get(p->p_children, child_idx);
    // Acquire child_proc->p_exit_lock
    lock_acquire(child_proc->p_exit_lock);
    if (child_proc->p_alive) {
      cv_wait(child_proc->p_exit_cv, child_proc->p_exit_lock);
    } 
    // When we reach here, we've woken by dead child process, (or it's already dead)
    // retrieve exitcode
    exitstatus = child_proc->p_exitcode;
    lock_release(child_proc->p_exit_lock);
    // Since we exhausted child process's use, we can destroy it
    proc_destroy(child_proc);
    // Remove it from parent-child relationship
    array_remove(p->p_children, child_idx);
  }
#endif

  result = copyout((void *)&exitstatus,status,sizeof(int));
  if (result) {
    return(result);
  }
  *retval = pid;
  return(0);
}

#if OPT_A2
int sys_fork(struct trapframe *tf, pid_t *retval) {
  struct proc *proc;
  struct addrspace *child_as;
  struct trapframe *tf_cp;

  proc = proc_create_runprogram("proc");
  // Signal failure to create process when proc is NULL
  if (proc == NULL) {
    return (EMPROC);
  }

  struct addrspace *cur_as = curproc_getas();
  if (cur_as == NULL) {
    return (EMPROC);
  }

  int as_cp_retval = as_copy(cur_as, &child_as);
  if (as_cp_retval != 0) {
    return (EMPROC);
  }

  proc->p_addrspace = child_as;

  // Copy parent trapframe onto OS heap so that
  // enter_forked_process can copy from without
  // considering when parent exits
  tf_cp = kmalloc(sizeof(struct trapframe));
  if (tf_cp == NULL) {
    return (EMPROC);
  }
  *tf_cp = *tf;

  // Assign parent-child relationship
  proc->p_parent = curproc;
  array_add(curproc->p_children, proc, NULL);

  // fork new thread for child process
  thread_fork("test_thread", proc, enter_forked_process, tf_cp, 0);
  // Return child's pid in sys_fork
  *retval = proc->pid;

  return (0);
}

static vaddr_t get_aligned_stackptr(const vaddr_t stackptr, size_t num_args) {
  vaddr_t top_of_stack = stackptr;
  while (top_of_stack % 4 != 0) --top_of_stack;
  top_of_stack -= sizeof(vaddr_t);
  if ((top_of_stack - num_args * sizeof(vaddr_t)) % 8 != 0) {
    top_of_stack -= sizeof(vaddr_t);
  }
  return top_of_stack;
}

int sys_execv(userptr_t prog, userptr_t args) {
  int result = 0;
  int argc = 0;
  char progname[128];
  
  struct addrspace *as, *old_as;
	struct vnode *v;
	vaddr_t entrypoint, stackptr;
  
  // Copy progname
  result = copyinstr(prog, progname, 128, NULL);
  if (result) {
    return (result);
  }

  // Count # of args
  while (true) {
    char *arg = NULL;
    userptr_t usrptr = (userptr_t)(((char**)args)[argc]);
    result = copyin(usrptr, &arg, sizeof(char*));
    if (!arg) break;
    ++argc;
  }

  char args_array[argc][129];
  for (int i = 0; i < argc; ++i) {
    userptr_t usrptr = (userptr_t)(((char**)args)[i]);
    result = copyinstr(usrptr, args_array[i], 129, NULL);
    if (result) {
      return (result);
    }
  }

	/* Open the file. */
	result = vfs_open(progname, O_RDONLY, 0, &v);
	if (result) {
		return result;
	}

	/* Create a new address space. */
	as = as_create();
	if (as ==NULL) {
		vfs_close(v);
		return ENOMEM;
	}

	/* Switch to it and activate it. */
	old_as = curproc_setas(as);
	as_activate();

	/* Load the executable. */
	result = load_elf(v, &entrypoint);
	if (result) {
		/* p_addrspace will go away when curproc is destroyed */
		vfs_close(v);
		return result;
	}

	/* Done with the file now. */
	vfs_close(v);

	/* Define the user stack in the address space */
	result = as_define_stack(as, &stackptr);
	if (result) {
		/* p_addrspace will go away when curproc is destroyed */
		return result;
	}

  // add stack content
  vaddr_t tos = stackptr, bos = stackptr;
  for (int i = argc; i > 0; --i) {
    char *s = (i == 0) ? progname : args_array[i-1];
    size_t len = strlen(s) + 1;
    tos -= len;
    result = copyoutstr(s, (userptr_t) tos, len, NULL);
    if (result) {
      return (result);
    }
  }

  tos = get_aligned_stackptr(tos, argc+1);

  for (int i = argc+1; i > 0; --i) {
    if (i == argc+1) {
      void *nullptr = NULL;
      copyout(&nullptr, (userptr_t)tos, sizeof(void*));
      continue;
    }
    char *s = (i == 0) ? progname : args_array[i-1];
    size_t len = strlen(s) + 1;
    tos -= sizeof(vaddr_t);
    bos -= len;
    result = copyout((void*)(&bos), (userptr_t)tos, sizeof(vaddr_t));
    if (result) {
      return (result);
    }
  }

  as_destroy(old_as);

	/* Warp to user mode. */
	enter_new_process(argc /*argc*/, (userptr_t)tos /*userspace addr of argv*/,
			  (vaddr_t)tos, entrypoint);

	/* enter_new_process does not return. */
	panic("enter_new_process returned\n");
	return EINVAL;
}
#endif
